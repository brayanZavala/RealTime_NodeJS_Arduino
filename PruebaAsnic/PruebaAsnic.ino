


const int buttonPin = 8;    // the number of the pushbutton pin
const int ledPin = 13;      // the number of the LED pin
const int pwmPin = 6;

int ledState = HIGH;         // the current state of the output pin
int buttonState;             // the current reading from the input pin
int lastButtonState = LOW;   // the previous reading from the input pin
int LedVal = 0;
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long lastPressTime = 0;
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
unsigned long pressCount = 0;
unsigned long TimeElapsed = 0;


void setup() {
  Serial.begin(9600);
  pinMode(buttonPin, INPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(pwmPin, OUTPUT);
  
  // set initial LED state
  digitalWrite(ledPin, ledState);
 //digitalWrite(pwmPin, 1);
}

void loop() {
  
  int reading = digitalRead(buttonPin);
  int Temp = map(analogRead(A0),0,1023,0,100); 

  if(Serial.available() > 1)
    LedVal =  Serial.readString().toInt();
  
  if (reading != lastButtonState)
    lastDebounceTime = millis();

  unsigned long now = millis() - lastDebounceTime;
  if (now > debounceDelay) 
  {
    if (reading != buttonState)
    {
      buttonState = reading;
      
      if (buttonState == HIGH) 
       {
        ledState = !ledState;
        pressCount++;
        TimeElapsed = millis() - lastPressTime;
        lastPressTime = millis();
       }
    }
  }

  digitalWrite(ledPin, ledState);
  lastButtonState = reading;
  Serial.println(String(pressCount) + "," + String(TimeElapsed) + "," + String(Temp));
  analogWrite(pwmPin,LedVal);
  delay(10);
}
