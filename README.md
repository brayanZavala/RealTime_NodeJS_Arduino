## PRUEBA DE INGRESO ASNIC

### DESCRIPCION:
Aplicacion arduino que sensa entradas analogas y digitales y comparte la informacion con una pagina web donde dicha informacion sera graficada.

### REQUISITOS  
- La placa sensara un boton como entrada digital
- La placa sensara una sensor de temperatura como entrada analoga
- Sera registrado el numero de veces que se ha presionado el boton 
- Sera registrado el lapso de tiempo entre los cuales el boton ha sido presionado en formato mm:ss
- Toda esta informacion sera registrada en una pagina web que graficara los resultados

### PLATAFORMA
- La logica de la placa arduino esta en C++ en su IDE nativo
- La pagina se creo con Node.js

### INSTRUCCIONES
- Una ves descargado el repositorio abrir la consola y ejecutar  **npm i**  (se necesita Node.js instalado) esto descargara automaticamente las dependencias usadas en el proyecto
- Situarse en la carpeta del proyecto y ejecutar **node index.js** en la terminal
- Abrir el navegador y acceder a **localhost:3000**
- Disfrutar :) 


