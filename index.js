//Importando dependencias
var express = require('express');
var app = express();
var server = require('http').createServer(app);
//Inicimos los sockets para conectarnos con el cliente
var io = require('socket.io', { rememberTransport: false, transports: ['WebSocket', 'Flash Socket', 'AJAX long-polling'] }).listen(server);
var sockets = [];
//Importamos la libreria SerialPort
var serialPort = require("serialport");
var Serial = serialPort;

//Configuramos el puerto serial "COM9" a 9600 baudios y salto de linea el mismo que en arduino 
var sPort = new Serial("COM9", {
    baudrate:9600,
    parser: serialPort.parsers.readline("\n")
});

//Iniciamos el servidor
server.listen(process.env.PORT || 3000);
console.log('Servidor iniciado...');

app.use("/", express.static(__dirname + "/"))

//Eventos express
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

//Se ejecuta cuando el puerto serial se abre
sPort.on("open", () => {
    console.log("Arduino conectado");
})

//Se ejecuta cuando llega un dato por el puerto serial
sPort.on("data", (data) => {
    var datos = data.split(",");
    io.sockets.emit("lectura", {count: datos[0], time: datos[1], temp: datos[2]});
});

//Conectandose con el cliente via sockets
io.on("connection", (socket) => {
    sockets.push(sockets);
    console.log("Clientes conectados: " + sockets.length);

    socket.on("updateLed",(data) => {
        sPort.write(data.toString());
    });
});

